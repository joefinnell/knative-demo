# GitLab FaaS Demo

## Workflow
1. Create a new branch
2. Create new function/update existing function (update serveless.yml if new function)
3. Push changes to branch (deployments will only push to development enviornment)
4. Use development URL to test new function changes
5. Merge to Master (deployments will push to development and production enviornments)

## Demo Functions

View existing functions in the operations->serverless

* `echo-js` uses node built in runtime from gitlab for a simple echo POST call
* `echo-php` generic hello world example built in php 7.3 with custom runtime

## echo-js curl

### Production Enviornment echo-js:

    curl -v --header "Content-Type: application/json" --data '{"Foo":"Bar"}' http://functions-echo-js.knative-demo-20606805-production.joefinnell.dev

### Development Enviornment echo-js:

    curl -v --header "Content-Type: application/json" --data '{"Foo":"Bar"}' http://functions-echo-js.knative-demo-20606805-development.joefinnell-test.dev

## echo-php curl

### Production Enviornment echo-php:

    curl http://functions-echo-php.knative-demo-20606805-production.joefinnell.dev

### Development Enviornment echo-php:

    curl http://functions-echo-php.knative-demo-20606805-development.joefinnell-test.dev


## Gitlab Serverless Documentation
- [Main Documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/)
- [Deploying Functions](https://docs.gitlab.com/ee/user/project/clusters/serverless/#functions)
- [Serveless.yml](https://docs.gitlab.com/ee/user/project/clusters/serverless/#functions)